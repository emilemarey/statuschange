package com.loni.statuschange.controller;


import com.loni.statuschange.entities.TaskEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatusChangeController {

    @PostMapping("/devices/{deviceId}")
    public ResponseEntity<TaskEntity> setDeviceStatus(@PathVariable("deviceId") final String deviceId,
                                                  @RequestBody TaskEntity taskEntity){
        System.out.println("############ DeviceId:" + taskEntity.toString());
        return new ResponseEntity<>(taskEntity, HttpStatus.OK);
    }

}
