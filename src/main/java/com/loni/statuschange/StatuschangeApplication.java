package com.loni.statuschange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatuschangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(StatuschangeApplication.class, args);
	}

}
